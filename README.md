# Modul1-rev

Refleksi 1
- masih bisa diimprove buat handle input string di quantity

refleksi 2
-

# Modul 2 refleksi

Tidak Mengisi Refleksi

# Modul 3 refleksi

Have you implemented SRP? If yes, explain the principle; if not, please change your code as per SRP. Keep in mind that SRP is a class that has only one reason to change. In other words, a class should have only one responsibility or encapsulate only one aspect of the software's functionality.

- car dan Product.java masing-masing telah mengenkapsulasi property dan behaviour dari entitinya masing-masing, sehingga setiap class hanya memiliki satu responsibility saja.

Have you implemented OCP? If yes, explain the principle, if not, please modify your code as per OCP. Remember that OCPs are software entities (classes, modules, functions, etc.) that should be open for development but closed for modification. This means you should be able to extend a module's behavior without changing its source code.

- Dengan mengimplementasi IRepository, CarRepository dan ProductRepository.java, kita dapat menambah fitur class baru tanpa mengubah behavior yang ada.

Have you implemented LSP? If yes, explain the principle; if not, modify your code to suit the LSP. It should be remembered that the LSP is an object from the superclass that must be replaced with an object from the subclass without affecting the correctness of the program. In other words, subclasses must be replaceable with their base class without changing desired program properties, such as correctness and consistency.

- dengan dimodifikasinya CarService, implementasi subclass dapat diubah tanpa mempengaruhi correctness dari program.

Have you implemented an ISP? If yes, explain the principle; if not please modify your code according to ISP. Keep in mind that ISPs recommend that large interfaces be broken down into smaller, more specific interfaces so that clients only need to know the methods that are relevant to them.

- dengan dimodifikasinya CarService, ini akan memprevent client sehingga tidak akan bergantung ke method yang tidak akan mereka gunakan.

Have you implemented DIP? If yes, explain the principle; if not change your code as per DIP. Note that DIP recommends that high-level modules do not depend on low-level modules. Both must rely on abstractions. Additionally, abstraction should not depend on details; details must rely on abstraction.

- IRepository merupakan implementasi dari DIP, yang memastikan service layer independent terhadap data layer masing-masing class.

Advantages

- Dengan mengaplikasikan SRP, kita memastikan setiap class hanya memiliki satu responsibility. Sehingga lebih mudah untuk di-mantain. Contohnya pada class Car dan Product.java.
- Dengan adanya OCP, memungkinkan penambahan fitur baru dengan sedikit perubahan pada skeleton code, sehingga tinggal hanya mengextend interface saja. Contohnya pada CarRepository yang meng-extend IRepository.

Disadvantages

- Penerapan DIP dan ISP dapat meningkatkan kompleksitas. contohnya jika terdapat interface kecil yang banyak, akan lebih sulit untuk mengtrack service mana yang mengimplementasikan fungsi tertentu.
- Penerapan semua prinsip SOLID memperlukan waktu yang lebih banyak untuk merancangnya. Perlu untuk membuat banyak interface dan juga memastikan DI dilakukan dengan benar.
