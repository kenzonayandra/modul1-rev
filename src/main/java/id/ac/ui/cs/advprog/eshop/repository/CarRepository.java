package id.ac.ui.cs.advprog.eshop.repository;

import id.ac.ui.cs.advprog.eshop.model.Car;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class CarRepository implements IRepository<Car> {
    private List<Car> carData = new ArrayList<>();

    @Override
    public Car save(Car car) {
        if(car.getCarId() == null || car.getCarId().isEmpty()) {
            car.setCarId(UUID.randomUUID().toString());
        }
        carData.removeIf(existingCar -> car.getCarId().equals(existingCar.getCarId()));
        carData.add(car);
        return car;
    }

    @Override
    public List<Car> findAll() {
        return new ArrayList<>(carData);
    }

    @Override
    public Optional<Car> findById(String id) {
        return carData.stream()
                .filter(car -> id.equals(car.getCarId()))
                .findFirst();
    }

    @Override
    public void deleteById(String id) {
        carData.removeIf(car -> id.equals(car.getCarId()));
    }
}
