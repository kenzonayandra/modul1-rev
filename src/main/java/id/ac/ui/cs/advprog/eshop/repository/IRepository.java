package id.ac.ui.cs.advprog.eshop.repository;

import java.util.List;
import java.util.Optional;

public interface IRepository<T> {
    T save(T entity);
    List<T> findAll();
    Optional<T> findById(String id);
    void deleteById(String id);
}
