package id.ac.ui.cs.advprog.eshop.service;

import id.ac.ui.cs.advprog.eshop.model.Car;
import id.ac.ui.cs.advprog.eshop.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car create(Car car) {
        return carRepository.save(car);
    }

    @Override
    public List<Car> findAll() {
        List<Car> allCar = new ArrayList<>();
        carRepository.findAll().forEach(allCar::add);
        return allCar;
    }

    @Override
    public Car findById(String carId) {
        return carRepository.findById(carId).orElse(null);
    }

    @Override
    public void update(String carId, Car car) {
        Car existingCar = findById(carId);
        if (existingCar != null) {
            car.setCarId(carId);
            carRepository.save(car);
        }
    }
    @Override
    public void deleteCarById(String carId) {
        carRepository.deleteById(carId);
    }
}
